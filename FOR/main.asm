section .text
	global _start		; required for nasm & ld

_write:
	MOV AL, 0x1		; put 1 in lower AX
	MOV DL, 0x1		; put 1 in lower DL
	MOV DIL, 0x1		; put 1 in lower RDI
	SYSCALL			; call SYS_WRITE
	RET			; return to where we came from. 
_start:
	MOV RAX, 0x0A7856 	; String to loop through, Vx\n
	PUSH RAX 		; put it on stack
	XOR RAX, RAX 		; clean RAX
	MOV RSI, RSP 		; move stack pointer to RSI
	JMP _check   		; Check first.
_action:
	CALL _write		; print it,
	INC RSI			; increase pointer.
_check:
	CMP BYTE [RSI], 0X0A 	; check if newline
	JNE _action		; if not newline, write!
	CALL _write		; is newline, but we want to include it, so print!
_quit:
	MOV RAX, 0x3C		; quit
	SYSCALL			; call sys_quit
