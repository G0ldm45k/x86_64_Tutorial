section .text
	global _start

_printStuff:
	MOV RAX, 0x1	; value for sys_write
	MOV RSI, RSP	; pointer to stack for characters
	MOV RDI, 0x1	; sys_out pipe
	SYSCALL		; call sys_write
	JMP _quit	; exit
_equal:
	PUSH 0x0A6C7145 ; push Eql on stack
	MOV RDX, 0x4	; Eql\n is 4 big
	JMP _printStuff; call _print
_bigger:
	PUSH 0x0A676942 ; push Big on stack
	MOV RDX, 0x4	; Big\n is 4 size
	JMP _printStuff ; call _print
_smaller:
	MOV R15, 0x0A6c6c616D53 ; push Small\n on stack
	PUSH R15	;
	MOV RDX, 6	; Small\n is 6
	JMP _printStuff	; print!
_quit:
	MOV RAX, 60 	; SYS_EXIT
	MOV RSI, 0	; ERROR CODE 0
	SYSCALL		; Call sys_exit
_start:
	MOV RAX, 12	; Put value in RAX
	MOV RBX, 13	; put value in RBX
	CMP RAX, RBX 	; what is RAX CMP 2 RBX
	JE _equal	; if equal
	JG _bigger	; if bigger
	JL _smaller	; if smaller

